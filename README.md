# Ido-Dictionaries

Various language dictionaries

These dictionaries are used in the Nia Ido vortolibro:
http://ayadan.moosader.com/gadgets/ido-vortolibro/


* English - Ido - http://www.idolinguo.org.uk/eniddyer.htm
* Ido - English - http://www.idolinguo.org.uk/idendyer.htm
* Español - Ido - http://kanaria1973.ido.li/krayono/dicespido.html
* Ido - Español - http://kanaria1973.ido.li/krayono/dicidoesp.html
* 日本語 - Ido - http://kanaria1973.ido.li/dicjaponaido.html
* Ido - 日本語 - http://kanaria1973.ido.li/dicidojapona.html
* Deutsch - Ido - http://idolinguo.org.uk/idger.htm
* Ido - Deutsch - http://kanaria1973.ido.li/dicidogermana.html
* Ido - Français - http://kanaria1973.ido.li/dicidofranca.html
* Ido - русский - http://kanaria1973.ido.li/krayono/dicidorusa.html
* Ido - Italiano - http://kanaria1973.ido.li/dicidoitaliano.html
* Ido - Portugues - http://kanaria1973.ido.li/dicidoportugues.html
* Ido - Nederlands - http://kanaria1973.ido.li/dicidonederlandana.html
* Ido - Suomi - http://kanaria1973.ido.li/dicidofinlandana.html
* Ido - Esperanto - http://kanaria1973.ido.li/dicidoesperanto.html
* Esperanto - Ido (kanaria, but from wayback machine)
* Ido - Interlingua (kanaria, but from wayback machine)
